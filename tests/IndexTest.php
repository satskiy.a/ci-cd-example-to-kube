<?php

// simple test that we have "Hello World"

use PHPUnit\Framework\TestCase;

class IndexTest extends TestCase {
    public function testHelloWorldOutput() {
        // Capture the output of the code
        ob_start();
        include 'index.php';
        $output = ob_get_clean();

        // Define the expected prefix
        $expectedPrefix = "Hello World";

        // Assert that the output starts with "Hello World"
        $this->assertStringStartsWith($expectedPrefix, $output);
    }
}